FROM python:3.11-alpine

EXPOSE 4785

COPY matrix_webhook.py requirements.txt /app/
COPY middleware/__init__.py /app/middleware.py

WORKDIR /app

RUN pip3 install --no-cache-dir -r requirements.txt

CMD ["./matrix_webhook.py"]
