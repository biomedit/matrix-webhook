#!/usr/bin/env python3
"""
Matrix Webhook.

Post a message to a matrix room with a simple HTTP POST
v1: matrix-client & http.server
v2: matrix-nio & aiohttp & markdown
"""

import asyncio
import json
import logging
import os
import urllib.parse
from base64 import b64decode
from http import HTTPStatus
from signal import SIGINT, SIGTERM

from aiohttp import web
from markdown import markdown
from nio import AsyncClient
from nio.exceptions import LocalProtocolError

# Customizable middleware. Override with your stuff:
from middleware import json_middlewares

SERVER_ADDRESS = (os.environ.get("HOST", ""), int(os.environ.get("PORT", 4785)))
MATRIX_URL = os.environ.get("MATRIX_URL", "https://matrix.org")
MATRIX_ID = os.environ.get("MATRIX_ID", "@wwm:matrix.org")
MATRIX_PW = os.environ["MATRIX_PW"]
API_KEY = os.environ["API_KEY"]
API_KEY_FIELD = os.environ.get("API_KEY_FIELD", "key")
ROOM_FIELD = os.environ.get("ROOM_FIELD", "room")
LOG_LEVEL = os.environ.get("LOG_LEVEL", "INFO")

CLIENT = AsyncClient(MATRIX_URL, MATRIX_ID)

logging.basicConfig(level=getattr(logging, LOG_LEVEL))


async def handler(request: web.Request):
    """
    Coroutine given to the server, st. it knows what to do with an HTTP request.

    This one handles a POST, checks its content, and forwards it to the matrix room.
    """
    data = await request.text()
    content_type = request.content_type or guess_content_type(data)
    if content_type == "application/x-www-form-urlencoded":
        try:
            data = urllib.parse.parse_qs(data, strict_parsing=True, max_num_fields=1)
            data = {key: value[0] for key, value in data.items()}
        except ValueError as e:
            logging.error(f"Invalid urlencoded data: {e}\n" + data)
            return create_json_response(
                HTTPStatus.BAD_REQUEST, "Invalid urlencoded data"
            )
    elif content_type == "text/plain":
        data = {"text": data}
    else:
        try:
            data = json.loads(data)
        except json.decoder.JSONDecodeError as e:
            logging.error(f"Invalid json: {e}\n" + data)
            return create_json_response(HTTPStatus.BAD_REQUEST, "Invalid JSON")

    for modifyer in json_middlewares:
        data = modifyer(data)

    try:
        api_key = data.pop(API_KEY_FIELD)
    except KeyError:
        api_key = decode_authorization_header(request.headers)

    if api_key != API_KEY:
        return create_json_response(HTTPStatus.UNAUTHORIZED, "Invalid " + API_KEY_FIELD)

    room_id = request.path.lstrip("/") or data.pop(ROOM_FIELD, "")
    if not room_id:
        return create_json_response(
            HTTPStatus.BAD_REQUEST, "Missing key: " + ROOM_FIELD
        )

    if "text" not in data:
        text = json.dumps(data, indent=2)
        formatted_body = markdown(f"```json\n{text}\n```", extensions=["fenced_code"])
    else:
        text = data["text"]
        formatted_body = data.get(
            "formatted_text", markdown(text, extensions=["extra"])
        )
    content = {
        "msgtype": "m.text",
        "body": text,
        "format": "org.matrix.custom.html",
        "formatted_body": formatted_body,
    }
    try:
        await send_room_message(room_id, content)
    except LocalProtocolError:  # Connection lost, try another login
        await CLIENT.login(MATRIX_PW)
        await send_room_message(room_id, content)

    return create_json_response(HTTPStatus.OK, "OK")


def guess_content_type(payload: str) -> str:
    """Based on the payload, guess the content type.

    Either application/json or application/x-www-form-urlencoded."""
    if payload.startswith("{"):
        return "application/json"
    return "application/x-www-form-urlencoded"


def decode_authorization_header(headers):
    auth_header = headers.get("Authorization", "")
    try:
        return b64decode(auth_header.split()[-1]).decode().split(":")[0]
    except IndexError as e:
        logging.error(
            "Authorization header missing or invalid: %s (%s)", auth_header, e
        )
        return None


def create_json_response(status, ret):
    """Create a JSON response."""
    response_data = {"status": status, "ret": ret}
    return web.json_response(response_data, status=status)


async def send_room_message(room_id, content):
    """Send a message to a room."""
    return await CLIENT.room_send(
        room_id=room_id, message_type="m.room.message", content=content
    )


async def main(event):
    """
    Launch main coroutine.

    matrix client login & start web server
    """
    await CLIENT.login(MATRIX_PW)

    server = web.Server(handler)
    runner = web.ServerRunner(server)
    await runner.setup()
    site = web.TCPSite(runner, *SERVER_ADDRESS)
    await site.start()

    # Run until we get a shutdown request
    await event.wait()

    # Cleanup
    await runner.cleanup()
    await CLIENT.close()


def terminate(event, signal):
    """Close handling stuff."""
    event.set()
    asyncio.get_event_loop().remove_signal_handler(signal)


def run():
    """Launch everything."""
    loop = asyncio.get_event_loop()
    event = asyncio.Event()

    for sig in (SIGINT, SIGTERM):
        loop.add_signal_handler(sig, terminate, event, sig)

    loop.run_until_complete(main(event))

    loop.close()


if __name__ == "__main__":
    run()
